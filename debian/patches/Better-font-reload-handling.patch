From 540b82ac07c5a582da3d6e6ad1cdc2fd83c36b2e Mon Sep 17 00:00:00 2001
From: Zhang Boyang <zhangboyang.id@gmail.com>
Date: Mon, 23 May 2022 14:10:52 +0800
Subject: [PATCH v4 1/4] Better font reload handling

Previous font reload code will leak a mmap on each reload. This patch
adds the ability to munmap old font after reload. However, this also
introduces a bug, if font reload is triggered while drawing in progress,
after signal handler returns, the drawing code will continue to use old
font which has been freed, causing crash. So the munmap is temporarily
disabled until we fix async-signal-safety problems completely.
---
 bogl-bgf.c | 63 +++++++++++++++++++++++++++++++++++++++++++-----------
 bogl-bgf.h |  1 +
 bterm.c    |  9 ++++----
 3 files changed, 55 insertions(+), 18 deletions(-)

diff --git a/bogl-bgf.c b/bogl-bgf.c
index 1032028..beed3c8 100644
--- a/bogl-bgf.c
+++ b/bogl-bgf.c
@@ -5,38 +5,55 @@
 #include <sys/stat.h>
 #include <sys/types.h>
 #include <unistd.h>
+#include <stddef.h>
 
 #include "bogl.h"
 #include "boglP.h"
 #include "bogl-font.h"
+#include "bogl-bgf.h"
+
+struct bogl_bgf {
+  void *f;    /* mmap area */
+  off_t size; /* size of mmap area */
+  struct bogl_font font; /* font descriptor */
+};
+#define bgf_of(font) \
+  ((struct bogl_bgf *)((char *)(font) - offsetof(struct bogl_bgf, font)))
 
 struct bogl_font *bogl_mmap_font(char *file)
 {
-  int fd;
+  struct bogl_bgf *bgf = NULL;
+  struct bogl_font *font = NULL;
+  int fd = -1;
   struct stat buf;
-  void *f;
-  struct bogl_font *font;
+  void *f = MAP_FAILED;
+
+  bgf = (struct bogl_bgf *)malloc(sizeof(struct bogl_bgf));
+  if (!bgf)
+    goto fail;
+  font = &bgf->font;
 
   fd = open(file, O_RDONLY);
   if (fd == -1)
-    return 0;
+    goto fail;
 
   if (bogl_cloexec(fd) < 0)
-    return 0;
+    goto fail;
 
   if (fstat(fd, &buf))
-    return 0;
+    goto fail;
+  bgf->size = buf.st_size;
+
+  if (buf.st_size < 4)
+    goto fail;
 
   f = mmap(0, buf.st_size, PROT_READ, MAP_SHARED, fd, 0);
-  if (f == (void *)-1)
-    return 0;
+  if (f == MAP_FAILED)
+    goto fail;
+  bgf->f = f;
 
   if (memcmp("BGF1", f, 4))
-    return 0;
-
-  font = (struct bogl_font *)malloc(sizeof(struct bogl_font));
-  if (!font)
-    return 0;
+    goto fail;
 
   memcpy(font, f + 4, sizeof(*font));
   font->name = ((void *)font->name - (void *)0) + f;
@@ -44,5 +61,25 @@ struct bogl_font *bogl_mmap_font(char *file)
   font->index = ((void *)font->index - (void *)0) + f;
   font->content = ((void *)font->content - (void *)0) + f;
 
+done:
+  if (fd != -1)
+    close(fd);
   return font;
+
+fail:
+  if (bgf) {
+    free(bgf);
+    bgf = NULL;
+    font = NULL;
+  }
+  if (f != MAP_FAILED)
+    munmap(f, buf.st_size);
+  goto done;
+}
+
+void bogl_munmap_font(struct bogl_font *font)
+{
+  struct bogl_bgf *bgf = bgf_of(font);
+  munmap(bgf->f, bgf->size);
+  free(bgf);
 }
diff --git a/bogl-bgf.h b/bogl-bgf.h
index e9fb994..f14a260 100644
--- a/bogl-bgf.h
+++ b/bogl-bgf.h
@@ -1,2 +1,3 @@
 
 struct bogl_font *bogl_mmap_font(char *file);
+void bogl_munmap_font(struct bogl_font *font);
diff --git a/bterm.c b/bterm.c
index 605644f..dfae8b9 100644
--- a/bterm.c
+++ b/bterm.c
@@ -224,11 +224,10 @@ void reload_font(int sig)
       return;
     }
   
-  /* This leaks a mmap.  Since the font reloading feature is only expected
-     to be used once per session (for instance, in debian-installer, after
-     the font is replaced with a larger version containing more characters),
-     we don't worry about the leak.  */
-  free(term->font);
+  /* BUG: Unmapping old font in this signal handler may cause crash if
+     drawing is in progress, so disable this temporarily until we fix
+     async-signal-safety problems completely. */
+  //bogl_munmap_font(term->font);
 
   term->font = font;
   term->xstep = bogl_font_glyph(term->font, ' ', 0);
-- 
2.30.2

